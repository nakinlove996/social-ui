import './register.css'

export default function Register() {
    return (
        <div className="login">
            <div className="loginWrapper">
                <div className="loginLeft">
                    <h3 className="loginLogo">NakinApp</h3>
                    <span className="loginDesc">Connect with friend and the world around you on NakinApp</span>
                </div>
                <div className="loginRight">
                    <div className="loginBox">
                        <input type="text" placeholder="Usernam" className="loginInput" />
                        <input type="text" placeholder="Email" className="loginInput" />
                        <input type="password" placeholder="Password" className="loginInput" />
                        <input type="password" placeholder="Password Again" className="loginInput" />
                        <button className="loginButton">Sing Up</button>
                        <div className="loginHr"></div>
                        <button className="loginRegisterButton">
                            Log In Account
                        </button>
                    </div>
                </div>
            </div>
        </div>
    )
}
