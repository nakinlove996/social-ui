import "./topbar.css"
import SearchIcon from '@material-ui/icons/Search';
import PersonIcon from '@material-ui/icons/Person';
import ChatIcon from '@material-ui/icons/Chat';
import NotificationsIcon from '@material-ui/icons/Notifications';
import {
    Link
  } from "react-router-dom";

export default function Topbar() {
    return (
        <div>
            <div className="topbarContainer">
                <div className="topbarLeft">
                    <span className="logo">Nakin</span>
                </div>
                <div className="topbarCenter">
                    <div className="searchbar">
                        <SearchIcon className="searchIcon"/>
                        <input placeholder="Search for friend, post or video" className="searchInput" />
                    </div>
                </div>
                <div className="topbarRight">
                    <div className="topbarLinks">
                        <Link to="/"><span className="topbarLink">Homepage</span></Link>
                        <span className="topbarLink">Timeline</span>
                    </div>
                    <div className="topbarIcons">
                        <div className="topbarIconsItems">
                            <PersonIcon />
                            <span className="topbarIconBadge">1</span>
                        </div>
                        <div className="topbarIconsItems">
                            <ChatIcon />
                            <span className="topbarIconBadge">2</span>
                        </div>
                        <div className="topbarIconsItems">
                            <NotificationsIcon />
                            <span className="topbarIconBadge">1</span>
                        </div>
                    </div>
                    <Link to="/profile"><img src="/assets/person/11.jpeg" alt="" srcset="" className="topbarImg" /></Link> 
                </div>
            </div>
        </div>
    )
}
