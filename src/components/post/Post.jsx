import './post.css'
import MoreVertIcon from '@material-ui/icons/MoreVert';
import {Users} from '../../dummyData'
import {useState} from 'react'

export default function Post({post}) {

    const user = Users.filter((u)=> u.id === post?.userId )[0].username;
    const userProfile = Users.filter((u)=> u.id === post?.userId )[0].profilePicture;
    const [like, setLike] = useState(post.like)
    const [isLike, setIsLike] = useState(false)
    const likeHandler = () => {
        setLike(isLike ? like-1 : like+1)
        setIsLike(!isLike)
    }

    return (
        <div className="post">
            <div className="postWrapper">
                <div className="postTop">
                    <div className="postTopLeft">
                        <img className="postProfileImg" src={userProfile} alt=""/>
                        <span className="psotUsername">{user}</span>
                        <span className="postData">{post.date}</span>
                    </div>
                    <div className="postTopRight">
                        <MoreVertIcon/>
                    </div>
                </div>
                <div className="postCenter">
                    <span className="postText">{post?.desc}</span>
                    <img className="postImg" src={post.photo} alt=""/>
                </div>
                <div className="postBottom">
                    <div className="postBottomLeft">
                        <img className="linkIcon" src="/assets/like.png" onClick={likeHandler} alt=""/>
                        <img className="linkIcon" src="/assets/heart.png" onClick={likeHandler} alt=""/>
                        <span className="postLinkCounter">{like} people like it</span>
                    </div>
                    <div className="postBottomRight">
                        <span className="postCommentText">{post.comment} Comments</span>
                    </div>
                </div>
            </div>
        </div>
    )
}
